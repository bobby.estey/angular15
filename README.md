# angular15

## Overview

- Angular (JavaScript Framework) is an application design framework and development platform for creating efficient and sophisticated Single Page Applications (SPA).
- Angular features are used to build modular, lightweight and reusable mobile applications.
- Can be used to create full stack applications making HTTP requests to a backend server, e.g. MEAN Stack
    - MongoDB - NoSQL Database
    - Express - Backend Framework
    - Angular - Frontend Framework
    - NodeJS - JavaScript Runtime
- Can be run on the server side with Angular Universal
- Full featured framework (Router, HTTP, Services, Dependencies, etc)
- Integrated TypeScript - Superset of JavaScript, Static Typing
- RxJS - efficient, asynchronous programming, observables, promises
- Test Friendly with both unit and end-to-end testing
- Enterprise Business Oriented, more strict and standardized

# References

- [Angular Docs](https://angular.io)
- [Introduction - Traversy](https://www.youtube.com/watch?v=3dHNOWTI7H8)
- [Font Awesome](https://github.com/FortAwesome/angular-fontawesome)
    - ng add @fortawesome/angular-fontawesome
    - select Free Solid Icons, Free Regular Icons and Free Brands Icons
- [JSON Server](https://www.npmjs.com/package/json-server)
    - Full REST API local server - mocked backend
    - npm i json-server
    - add to package.json "jsonServer": "json-server --watch jsonServerDatabase.json --port 5000"
    - npm run jsonServer
    - http://localhost:5000/tasks

## Installation

- Install Angular
   - npm install -g @angular/cli
   - ng version (get the version)

- Test Application
   - cd directory to create application
   - ng new applicationName (creates application)
   - ng serve -o (serve application)
   - browser should render Angular application with URI (localhost:4200)

## Component Driven

- index.html
    - points to the app-root selector
- app.component.ts
    - @Component decorator has the following parameters:
    - selector: 'app-root' (component element)
    - templateUrl: './app.component.html' (component view template)
    - styleUrls: './app.component.css' (component view styles)
    - providers: services - increase modularity and reusability, e.g. fetching data, validation, logging, etc.

## Definitions

- Definitions
    - Directory Definitions
        - [node_modules](https://www.freecodecamp.org/news/what-are-node-modules/) - reusable JavaScript code
            - built in - default out of the box when installing node.js
            - local - site specific code
            - third party - Node Package Manager (NPM Registry)
        - src - source files for root level
        - src/app - components
        - src/assets - index.html, images, etc.
        - src/environments - test directory
    - [File Definitions](https://angular.io/guide/file-structure)

## Overview

- What are the differences between TypeScript and ECMA JavaScript?
