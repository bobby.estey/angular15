import { Task } from './Task';
export const TASKS: Task[] = [
  {
    id: 1,
    text: 'Bobby',
    timestamp: '20230302',
    reminder: true,
  },
  {
    id: 2,
    text: 'Ester',
    timestamp: '20230303',
    reminder: true,
  },
  {
    id: 3,
    text: 'Rebby',
    timestamp: '20230304',
    reminder: false,
  },
];
