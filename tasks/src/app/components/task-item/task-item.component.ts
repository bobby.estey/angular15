import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Task } from "../../Task";
import { faTimes} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent  implements OnInit {

  @Input() task!: Task;
  @Output() onDeleteTask: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() onToggleReminder: EventEmitter<Task> = new EventEmitter<Task>();

  faTimes = faTimes;

  constructor() {
  }

  ngOnInit(): void {
    console.log('TaskItemComponent.ngOnInit()');
  }

  onDelete(task: Task) {
    console.log('TaskItemComponent.onDelete()');
    console.log(task);
    this.onDeleteTask.emit(task);
  }

  onToggle(task: Task) {
    console.log('TaskItemComponent.onToggle()');
    console.log(task);
    this.onToggleReminder.emit(task);
  }
}
