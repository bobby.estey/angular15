import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {Subscription} from "rxjs";
import {UiService} from "../../services/ui.service";
import {Task} from '../../Task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask: EventEmitter<Task> = new EventEmitter<Task>();

  subscription!: Subscription;
  text!: string;
  timestamp!: string;
  reminder: boolean = false;
  showAddTask!: boolean;

  constructor(private uiService: UiService) {
    this.subscription = this.uiService.onToggle().subscribe((value) => (this.showAddTask = value));
  }

  ngOnInit() {
    console.log("AddTaskComponent.ngOnInit");
  }

  onSubmit() {
    if(!this.text) {
      alert("Add a Task!");
      return;
    }

    const newTask = {
      text: this.text,
      timestamp: this.timestamp,
      reminder: this.reminder
    }

    this.onAddTask.emit(newTask);
    this.text = '';
    this.timestamp = '';
    this.reminder = false;
  }
}
