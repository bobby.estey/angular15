import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() text: string = "Add";
  @Input() color: string = "Green";
  @Output() btnClick = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
    console.log('ButtonComponent.ngOnInit()');
    // throw new Error('HeaderComponent.ngOnInit: Method not implemented.');
  }

  onClick() {
    console.log('ButtonComponent.onClick()');
    this.btnClick.emit();
  }
}
