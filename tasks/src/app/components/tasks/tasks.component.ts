import {Component, OnInit} from '@angular/core';
import {TaskService} from "../../services/task.service";
import {Task} from "../../Task";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent  implements OnInit {

  tasks: Task[] = [];

  constructor(private taskService: TaskService) {
  }

  ngOnInit(): void {
    console.log('TasksComponent.ngOnInit()');
    // subscribe to the tasks observable
    this.taskService.getTasks().subscribe((tasks) => this.tasks = tasks);
  }

  addTask(task: Task) {
    console.log('TasksComponent.addTask()');
    console.log(task);
    this.taskService.addTask(task).subscribe((task) => (this.tasks.push(task)));
  }

  deleteTask(task: Task) {
    console.log('TasksComponent.deleteTask()');
    console.log(task);
    this.taskService.deleteTask(task).subscribe(() => (this.tasks = this.tasks.filter(t => t.id !== task.id)));
  }

  toggleReminder(task: Task) {
    console.log('TasksComponent.toggleReminder()');
    console.log(task);
    task.reminder = !task.reminder;
    this.taskService.updateTaskReminder(task).subscribe();
  }
}
