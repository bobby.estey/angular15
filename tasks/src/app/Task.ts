export interface Task {
  id?: number;
  text: string;
  timestamp: string;
  reminder: boolean;
}
